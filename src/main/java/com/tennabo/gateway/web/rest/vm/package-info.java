/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tennabo.gateway.web.rest.vm;
